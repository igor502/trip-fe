import { Component } from '@angular/core';
import {Routes, RouterModule} from "@angular/router";
import {LoginFormComponent} from "./login-form/login-form.component";
import {TestComponent} from "./test/test.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent {
  title = 'app works!';
}
