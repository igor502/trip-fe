import { TripFePage } from './app.po';

describe('trip-fe App', () => {
  let page: TripFePage;

  beforeEach(() => {
    page = new TripFePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
